<?php

use Silex\Provider\MonologServiceProvider;
//use Silex\Provider\WebProfilerServiceProvider;

// include the prod configuration
require __DIR__.'/prod.php';

// enable the debug mode
$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/silex_dev.log',
));

$app['sql_data'] = [
	'host' => 'localhost',
	'user' => 'amxx_shop',
	'password' => 'amxx_shop',
	'database' => 'amxx_shop'
];

// $app->register(new WebProfilerServiceProvider(), array(
//     'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
// ));
