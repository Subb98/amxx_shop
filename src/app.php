<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class AMXXShopApp extends Silex\Application {
    use Silex\Application\UrlGeneratorTrait;
}

$app = new AMXXShopApp();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});

// creating password check service
$app['pass_encoder'] = function () {
    return new BCryptPasswordEncoder(4);
};

return $app;
