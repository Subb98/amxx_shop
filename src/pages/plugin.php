<?php
$app->get('/plugins/{id}', function($id) use($app, $is_admin) {
	$mysqli = SqlConnect();

	if($stmt = $mysqli->prepare("SELECT * FROM amxx_products WHERE id=?")) {
		$stmt->bind_param('d', $id);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows) {
			$plugin = $result->fetch_array();
		} else {
			$plugin = NULL;
		}

		$stmt->close();
	}

	return $app['twig']->render('plugin.twig', array(
		'subtitle' => $plugin['name'],
		'plugin' => $plugin,
		'is_admin' => $is_admin
	));
})->assert('id', '\d+')->bind('plugins');
