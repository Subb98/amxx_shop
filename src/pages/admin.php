<?php
$app->get('/admin', function() use($app, $is_admin) {
	if(!$is_admin) {
		return $app->redirect('/login');
	}

	return $app['twig']->render('admin.twig', array(
		'subtitle' => 'Админ-центр',
		'is_admin' => $is_admin
	));
})->bind('admin');

$app->get('/logout', function() use($app, $is_admin) {
	if($is_admin) {
		SetUserAdmin(false);
	}

	return $app->redirect($app->path('login'));
})->bind('logout');
