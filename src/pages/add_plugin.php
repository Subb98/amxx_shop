<?php
use Symfony\Component\HttpFoundation\Request;

function SqlCheckPlugin($mysqli, $name) {
	if($prepare = $mysqli->prepare("SELECT count(*) FROM amxx_products WHERE name=?")) {
		$prepare->bind_param('s', $name);
		$prepare->execute();
		$result = $prepare->get_result();
		$prepare->close();
	}
	return ($result->fetch_row());
}

function SqlPushPlugin($mysqli, $plugin) {
	global $message;

	$uploaddir = '/uploads/';

	$preview = $plugin['preview_obj']->getClientOriginalName();
	$info = new SplFileInfo($preview);
	$extension = '.' . $info->getExtension();
	$preview = md5($preview) . $extension;

	$preview_path = $uploaddir . $preview;

	$name = $plugin['name'];
	$version = $plugin['version'];
	$author = $plugin['author'];
	$price = $plugin['price'];
	$description = $plugin['description'];

	if(isset($plugin['source_obj'])) {
		$source = $plugin['source_obj']->getClientOriginalName();
		$source_path = $uploaddir . $source;

		$query = "INSERT INTO `amxx_products` (`name`, `version`, `author`, `price`, `description`, `preview`, `source`)
		VALUES ('$name', '$version', '$author', $price, '$description', '$preview_path', '$source_path')";
	} else {
		$query = "INSERT INTO `amxx_products` (`name`, `version`, `author`, `price`, `description`, `preview`)
		VALUES ('$name', '$version', '$author', $price, '$description', '$preview_path')";
	}

	if(($result = mysqli_query($mysqli, $query))) {
		$message = 'Плагин успешно добавлен в базу данных';

		$plugin['preview_obj']->move(__DIR__ . '/../../web/uploads/', $preview);

		if(isset($source)) {
			$plugin['source_obj']->move(__DIR__ . '/../../web/uploads/', $source);
		}
	} else {
		$message = 'Ошибка выполнения запроса';
	}
}

$message = '';

$app->get('/add_plugin', function () use ($app, $is_admin, $message) {
	if(!$is_admin) {
		return $app->redirect($app->path('login'));
	}

	return $app['twig']->render('add_plugin.twig', array(
		'subtitle' => 'Добавить плагин',
		'is_admin' => $is_admin,
		'message' => $message
	));
})->bind('add_plugin');

$app->post('/add_plugin', function (Request $request) use ($app, $is_admin, &$message) {
	$plugin = [
		'name' => $request->get('name'),
		'version' => $request->get('version'),
		'author' => $request->get('author'),
		'price' => (int) $request->get('price'),
		'description' => $request->get('description'),
		'preview_obj' => $request->files->get('preview'),
		'source_obj' => $request->files->get('source')
	];

	if(empty($plugin['name']) or empty($plugin['version']) or empty($plugin['author']) or empty($plugin['description']) or empty($plugin['preview_obj'])) {
		$message = 'Пожалуйста, заполните все поля';
	} else {
		$mysqli = SqlConnect();
		$count = SqlCheckPlugin($mysqli, $plugin['name']);

		switch($count[0]) {
			case 0: {
				SqlPushPlugin($mysqli, $plugin);
				break;
			}
			default: {
				$message = 'Плагин с таким именем уже существует';
			}
		}
	}

	return $app['twig']->render('add_plugin.twig', array(
		'subtitle' => 'Добавить плагин',
		'is_admin' => $is_admin,
		'message' => $message
	));
})->bind('add_plugin_post');
