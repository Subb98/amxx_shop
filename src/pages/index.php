<?php
use JasonGrimes\Paginator;

const PER_PAGE = 15;

function SqlGetPluginsNum($mysqli) {
	$result = @$mysqli->query("SELECT COUNT(*) FROM `amxx_products`");
	return $result->fetch_row()[0];
}

function SqlGetCurPlugins($mysqli, $offset, $per_page) {
	if($stmt = $mysqli->prepare("SELECT * FROM amxx_products LIMIT ?, ?")) {
		$stmt->bind_param('dd', $offset, $per_page);
		$stmt->execute();
		$result = $stmt->get_result();
		while($row = $result->fetch_array()) {
			$cur_plugins[] = $row;
		}
		$stmt->close();
	}

	return $cur_plugins;
}

function index_page($page = 1) {
	global $app, $is_admin;
	$mysqli = SqlConnect();

	$plugins_num = SqlGetPluginsNum($mysqli);
	$message = '';

	if(!$plugins_num) {
		$message = 'Записей не обнаружено';
	}

	$paginator = new Paginator($plugins_num, PER_PAGE, $page, '/page/(:num)');
	$offset = $paginator->getCurrentPageFirstItem() - 1;
	$cur_plugins = SqlGetCurPlugins($mysqli, $offset, PER_PAGE);

	return $app['twig']->render('index.twig', array(
		'subtitle' => 'Главная страница',
		'plugins' => $cur_plugins,
		'paginator' => $paginator,
		'message' => $message,
		'is_admin' => $is_admin
	));
}

$app->get('/', 'index_page');
$app->get('/page/{page}', 'index_page')->assert('page', '\d+');
