<?php
use Symfony\Component\HttpFoundation\Request;

$mysqli = SqlConnect();

function SqlSearch($mysqli, $query) {
	global $results;

	$query = trim($query);
	$query = mysqli_real_escape_string($mysqli, $query);
	$query = htmlspecialchars($query);

	if(!empty($query)) {
		if(strlen($query) < 3) {
			$message = '<p>Слишком короткий поисковый запрос.</p>';
		} else if(strlen($query) > 64) {
			$message = '<p>Слишком длинный поисковый запрос.</p>';
		} else {
			$q = "SELECT `id`, `name`, `author`, `price`, `preview` FROM `amxx_products` WHERE `name` LIKE '%$query%'";
			$results = mysqli_query($mysqli, $q);

			if(mysqli_affected_rows($mysqli) > 0) {
				$num = mysqli_num_rows($results);
				$message = '<p>По запросу <b>'.$query.'</b> найдено совпадений: '.$num.'</p>';
			} else {
				$message = '<p>По вашему запросу ничего не найдено.</p>';
			}
		}
	} else {
		$message = '<p>Задан пустой поисковый запрос.</p>';
	}

	return $message;
}

$results = 0;

$app->post('/search', function (Request $request) use ($app, $mysqli, &$results, $is_admin) {
	$query = $request->get('query');
	$message = SqlSearch($mysqli, $query);

	return $app['twig']->render('search.twig', array(
		'subtitle' => 'Поиск товаров',
		'results' => $results,
		'is_admin' => $is_admin,
		'message' => $message
	));
})->bind('search_post');
