<?php
use Symfony\Component\HttpFoundation\Request;

$app->get('/login', function () use ($app) {
	if(GetUserAdmin()) {
		return $app->redirect($app->path('admin'));
	}

	return $app['twig']->render('login.twig', array(
		'subtitle' => 'Страница авторизации',
		'message' => ''
	));
})->bind('login');

$app->post('/login', function (Request $request) use ($app) {
	$login = $request->get('login');
	$password = $request->get('password');

	$mysqli = SqlConnect();

	if($stmt = $mysqli->prepare("SELECT `password` FROM `amxx_users` WHERE `login` = ?")) {
	    $stmt->bind_param('s', $login);
	    $stmt->execute();
	    $result = $stmt->get_result();
	    $encoded_password = $result->fetch_row()[0];
	    $stmt->close();
	}

	if($app['pass_encoder']->isPasswordValid($encoded_password, $password, "")) {
		SetUserAdmin();
		return $app->redirect($app->path('admin'));
	}

	$message = '';

	if(!GetUserAdmin()) {
		$message = 'Данные авторизации неверны';
	}

	return $app['twig']->render('login.twig', array(
		'subtitle' => 'Страница авторизации',
		'message' => $message
	));
});
