<?php
function SqlConnect() {
	global $app;

	$mysqli = @new mysqli($app['sql_data']['host'], $app['sql_data']['user'], $app['sql_data']['password'], $app['sql_data']['database']);

	if($mysqli->connect_errno) {
		echo 'SQL connection error: ' . $mysqli->connect_errno;
		exit();
	}

	$mysqli->set_charset("utf8");
	return $mysqli;
}

session_start();

function GetUserAdmin() {
	if(isset($_SESSION['admin'])) {
		return true;
	}

	return false;
}

function SetUserAdmin($value = true) {
	if($value == true) {
		return ($_SESSION['admin'] = $value);
	}

	unset($_SESSION['admin']);
}

$is_admin = GetUserAdmin();
